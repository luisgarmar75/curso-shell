#!/bin/nash
#Programa para ejemplificar el uso de los arreglos
#Autor: Luis García

arregloNumeros=(1 2 3 4 5 6)
arregloCadenas=(Manzana, Pera, Melon, Sandia, Uvas, Fresas, Papaya, Nanchi)
arregloRangos=({A..Z} {10..20})

#Imprimir todos los arrelgos
echo "**Imprimir todos los arrelgos**"
echo "Arreglo de números: ${arregloNumeros[*]}"
echo "Arreglo de cadenas: ${arregloCadenas[*]}"
echo "Arreglo de rangoss: ${arregloRangos[*]}"
echo -e "\n"

#Imprimir el tamaño del arreglo
echo "**Imprimir el tamaño del arreglo**"
echo "Tamaño del arreglo de numeros: ${#arregloNumeros[*]}"
echo "Tamaño del arreglo de cadenas: ${#arregloCadenas[*]}"
echo "Tamaño del arreglo de rangoss: ${#arregloRangos[*]}"
echo -e "\n"

#Imprimir la posicion en el arreglo
echo "**Imprimir la posicion en el arreglo**"
echo "Posición del arreglo de numeros: ${arregloNumeros[2]}"
echo "Posición del arreglo de cadenas: ${arregloCadenas[1]}"
echo "Posición del arreglo de rangoss: ${arregloRangos[6]}"
echo -e "\n"

#Añadir o eliminar valores de un arreglo
echo "**Añadir o eliminar valores de un arreglo (arreglo de números)**"
arregloNumeros[7]=44
unset arregloNumeros[0]
echo "Arreglo modificado de numeros: ${arregloNumeros[*]}"
echo "Tamaño modificado de numeros: ${#arregloNumeros[*]}"
echo -e "\n"
