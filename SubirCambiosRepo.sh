#!/bin/bash
#Programa para subir cambios al repositorio
#Autor: Luis Garcia

opcionStatus=""
opcionTodosArchivos=""
mensajeCommit=""
nombreArchivo=""
rutaActual=`pwd`

echo -e "\nPrograma para subir shells al repositorio de GitLab"

#Pedir confirmacion para checar el estatus de los archivos
read -n1 -p "¿Desea ver el estatus de los archivos a subir? (S/N):" opcionStatus
if [ $opcionStatus = "S" ]; then
	echo -e "\n"
	git status
elif [ $opcionStatus != "S" ] && [ $opcionStatus != "N" ]; then
	echo -e "\n"
	echo -e "Opción incorrecta\nFin del programa... 0%"
	exit 0	
fi
	
#Pedir confirmacion si se subirán todos los archivos o indicar el archivo especifico
echo -e "\n"
read -n1 -p "¿Desea subir todos los archivos contenidos en el directorio '$rutaActual'? (S/N):" opcionTodosArchivos
if [ $opcionTodosArchivos = "S" ]; then
	echo -e "\n"
	git add .
	echo "Archivos agregados correctamente... 100%"
elif [ $opcionTodosArchivos != "S" ] && [ $opcionTodosArchivos != "N" ]; then
	echo -e "\n"
	echo -e "Opción incorrecta\nFin del programa... 0%"
	exit 0	
else
	echo -e "\n"
	read -p "Ingrese nombre de archivo específico a subir: " nombreArchivo
	if [ -f $nombreArchivo ]; then
		git add $nombreArchivo
        	echo -e "\n"
        	echo "Archivo '$nombreArchivo' agregado correctamente... 100%"
	else
        	echo -e "\n"
        	echo -e "El archivo indicado no existe\nFin del progrrama 0%"
		exit 0	
	fi
fi

#Pedir nombre de identificador del commit
read -p "Indica el mensaje para identificar el commit ha realizar: " mensajeCommit
git commit -m "$mensajeCommit"
echo -e "\n"
echo -e "Commit '$mensajeCommit' realizado corrrectamente... 100%\nIngrese credenciales para validar la subida de archvos: "

$(git push origin master)
if [ "$?" = "0" ]; then	
	echo -e "\n"
	echo -e "Push realizado correctamente... 100%"
	echo -e "\n"
	echo "Fin del programa, los archivos están en GitLab..."
	echo "Proceso realizado correctamente... 100%"
else
	echo -e "Error de credenciales incorrectas\nNo se realizó el Push de los archivos\nFin del programa 0%"
	exit 0
fi
