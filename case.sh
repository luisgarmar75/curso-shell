#!/bin/bash
#Programa para demostrar el uso de la sentencia case
#Autor: Luis Garcia

opcion=""

echo "Ejemplo de sentencia Case"
read -n1 -p "Ingresa una opcion de la A - Z: " opcion
echo -e "\n"

case $opcion in
	"A") echo "Operacion garda archivo";;
	"B") echo "Operacion eliminar archivo";;
	"[C-E]") echo "No está implementada la operación";;
	*) echo "Opción incorrecta"
esac
