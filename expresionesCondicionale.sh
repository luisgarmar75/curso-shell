#!/bin/bash
#Programa para demostrar el uso de las expresiones condicionales
#Autor: Luis Garcia

edad=0
pais=""
rutaArchivo=""


read -p "Ingresa edad: " edad
read -p "Ingreso pais: " pais
read -p "Ingresa la ruta para tu archivo: " rutaArchivo

echo -e "\nExpresiones condicionales con números"
if [ $edad -lt 18 ]; then
	echo "La persona es mayor de edad"
elif [ $edad -ge 10 ] && [ $edad -le 17 ]; then
	echo "la persona es un adolecente"
fi


echo -e "\nExpresiones condicionales con cadenas"
if [ $pais = "EEUU" ]; then
	echo "La persona es americana"
elif [ $pais = "Mexico" ] || [$pais = "Brazil" ]; then
	echo "la persona es latina"
else
	echo "La persona no vive en el continente Americano"

fi

echo -e "\nExpresiones condicionales con archivos"
if [ -d $rutaArchivo ]; then
	echo "La $rutaArchivo existe"
else 
	echo "La ruta o directorio no existe"
fi
