#!/bin/bash
#Autor: Luis Garcia
# Programa para demostrar el uso de la sentencia while

echo "Ejemplo de loops anidados"

for comand in $(ls)
do
	for nombre in {1..4}
	do
		echo "Nombre del archivo: $comand _ $nombre"
	done
done
	
