#!/bin/bash
#Programa para indicar el uso de la sentencia de iteración For
#Autor: Luis García

arregloNumeros=(1 2 3 4 5 6 7)

#Ejemplo de como iterar un arreglo de números
echo "**Ejemplo de como iterar un arreglo de números**"
for num in ${arregloNumeros[*]} 
do

	echo "Valor en arreglo: $num"

done
echo -e "\n"

#Ejemplo de como iterar un arreglo de cadenas
echo "**Ejemplo de como iterar un arreglo de cadenas**"
for nom in "México" "Brazil" "Colombia" "Perú" 
do

	echo "Nombre de país: $nom"

done
echo -e "\n"

#Ejemplo de como iterar archivos de un directorio
echo "**Ejemplo de como iterar archivos de un directorio**"
for file in *
do

	echo "Nombre de archivo: $file"

done
echo -e "\n"

#Ejemplo de como iterar usando un comando
echo "**Ejemplo de como iterar usando un comando**"
for comando in $(pwd)
do

	echo "Comando de la terminal: $comando"

done
echo -e "\n"

#Iterar con el form tradicional
echo "**Iterar con el form tradicional**"
for ((i=1; i<=10; i+=2))
do

	echo "Número: $i"

done
