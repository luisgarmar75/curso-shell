#!/bin/bash
#Autor: Luis Garcia
#Programa para ejemplificar el uso de Brask y Continue

echo "Sentencias break y continue"
for comand in $(ls)
do
	for numero in {1..4}
	do
		if [ $comand = "descarga.sh" ]; then
			break;
		elif [[ $comand == 4* ]]; then
			continue;
		else 
			echo "Nombre del archivo $comand con numero $numero"
		fi
	done
done
