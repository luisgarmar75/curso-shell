#!/bin/bash
#Autor: Luis Garcia
#Programa que permite manejar las utilidades de Oracle

opcion=0

while :
do

	#Cambiar la interfaz de la terminal
	clear
	#Desplegar el menú de opciones
	echo "------------------------------------------------------------"
	echo "ORACLEUTIL - Programa de utilidades de Oracle"
	echo "		 	 Menú Pruncipal				 "
	echo "------------------------------------------------------------"
	echo "1. Instalar Oracle"
	echo "2. Desinstalar Oracle"
	echo "3. Crear un respaldo"
	echo "4. Restaurar respaldo"
	echo "5. Salir"
	
	#Leer los datos del usuario
	read -n1 -p "Ingrese una opción [1-5]: " opcion
	
	#validar la opcion ingresada
	case $opcion in
		1)	echo -e "\nInstalar Oracle..."
			#Comando para instalar oracle
			sleep 3
			;;

	
		2)	echo -e "\nDesinstalar Oracle..."
			#Comando para desinstalar oracle
			sleep 3
			;;

		3) 	echo -e "\nCreando resplado de BD de Oracle..."
			#Comando para crear respaldo en oracle
			sleep 3
			;;

		4)	echo -e "\nRestaurando la BD deOracle..."
			#Comando paa restaurar oracle
			sleep 3
			;;

		5)	echo -e "\nSalir de programa..."
			#Comando para salir de programa
			sleep 3
			exit
			;;

		*) 	echo -e "\nOpción inválida, intente de nuevo"
			sleep 2
			;;
	esac
done
