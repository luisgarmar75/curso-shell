#!/bin/bash
#Autor: Luis Garcia
#Programa para ejemplificar el uso de archivos y directorios

echo "Bienvenido al programam de archivos y directorios"
 
if [ $1 == "d" ]; then
	mkdir -m 755 $2
	echo "Directorio creado correctamente"
	ls -la $2
elif [ $1 == "f" ]; then
	touch $2
	echo "Archivo vacío creado correctamente"
	ls -la $2
else 
	echo "Opcion inválida: $1 , solo f y d son aceptados"
fi
	

