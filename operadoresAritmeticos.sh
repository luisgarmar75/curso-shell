#!/bin/bash
#programa para ver todos los operadores aritmeticos
#Autor: Luis Garcia

numA=10
numB=4

echo "\nOperadores aritmeticos"
echo "Los numeros son: A= $numA y B= $numB"
echo "Sumar A + B: " $((numA + numB))
echo "Restar A - B: " $((numA - numB))
echo "Multiplicar A * B: " $((numA * numB))
echo "Dividir A / B: " $((numA / numB))
echo "Residuo A % B: " $((numA % numB))

echo -e "\nOperadore relacionales"
echo "Los numeros son: A= $numA y B= $numB"
echo "A mayor que B: " $((numA > numB))
echo "A menor que B: " $((numA < numB))
echo "A mayor igual que B: " $((numA >= numB))
echo "A menor igual que B: " $((numA <= numB))
echo "A igual que B: " $((numA == numB))
echo "A diferente que B: " $((numA != numB))

echo -e "\nOperadores de asignacion"
echo "Los numeros son: A= $numA y B= $numB"
echo "Sumar B: " $((numA += numB))
echo "Restar B: " $((numA -= numB))
echo "Multiplicar B: " $((numA *= numB))
echo "Dividir B: " $((numA /= numB))
echo "Residuo B: " $((numA %= numB))
 
