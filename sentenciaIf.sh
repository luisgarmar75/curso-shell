#!/bin/bash
#Pragrama para demostrar el uso de la sentencia If
#Autor: Luis Garcia

notaCLase=0
edad=0

echo "Ejemplo de sentencia If Else"
read -n1 -p "Indique cual es su nota de clases (1-9): " notaClase
echo -e "\n"
if (( $notaClase >= 7 )); then 
	echo "El alumno aprobó."
else 
	echo "El alumno reprobó"
fi


read -p  "Indique cual es su edad: " edad
if [ $edad -le 17 ]; then 
	echo "La perdona no puede votar."
else 
	echo "La persona puede votar"
fi
