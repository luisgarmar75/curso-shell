#!/bin/bash
#Pragrama para demostrar el uso de la sentencia If anidados
#Autor: Luis Garcia

notaClase=0
continua=""

echo "Ejemplod de sentencia If Else"
read -p  "Indique cual es su nota de clase (1-9): " notaClase

echo -e "\n"

if [ $notaClase -ge 7 ]; then 
	echo "La alumno aprobó."
	read -p "¿Segurás estudiando (s/n): " continua
	if [ $continua = "s" ]; then 
		echo "Bienvenido a la siguiente clase"
	else
		echo "Gracias por estudiar con nosotros"
	fi
else
	echo "El alumno reprobó"
fi
