#!/bin/bash
#Programa para ejemplificar el uso de iteración while
#Autor: Luis García

numero=1

# le = less than equal
# ne = not equals ~ !=

while [ $numero -ne 10 ]
do 

	echo "Imprimiendo $numero veces"
	numero=$(( $numero+1 ))
done
